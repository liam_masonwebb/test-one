﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_test_one
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 1;

            {
                Console.Clear();

                
                Console.WriteLine($"          MENU- please select a number               ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"    01. how many days have you been here for??   ");
                Console.WriteLine($"                              ");
                Console.WriteLine("");
                Console.WriteLine();
                Console.WriteLine($"Please select an option");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine("please enter your date of birth (MM/DD/YYYY)");
                    string birthDateString = Console.ReadLine();
                    DateTime birthDate;
                    if (DateTime.TryParse(birthDateString, out birthDate))
                    {
                        DateTime today = DateTime.Now;
                        Console.WriteLine("You are {0} days old", (today - birthDate).Days);
                    }
                    else Console.WriteLine("Incorrect date format!");
                } while (menu == 1) ;

                if (menu > 1)
                {
                    Console.WriteLine($"Please choose the correct option");
                    Console.WriteLine($"Press <ENTER> to return to the main menu");
                    Console.ReadLine();
                    menu = 5;
                    Console.Clear();
                }
            }
        }


    }
}



